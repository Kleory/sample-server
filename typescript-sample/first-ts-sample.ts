function sample(name: String, obj: MySample) {
  console.log(name);
}

class MySample {
  private name: string;
  constructor (name: string){
    this.name = name;
  }
}

sample("123", new MySample('789'));