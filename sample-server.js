var http = require("http");

http
  .createServer(function (req, res) {
    res.writeHead(200, { "Content-Type": "text/html" });
    switch (req.url) {
      case "/favicon.ico":
        res.end(getFavIcon());
        return;
      case "/":
        res.end(getIndexPage());
        return;
      case "/inner":
        res.end(getInnerPage());
        return;
      default:
        console.log(req.url);
        res.end(get404Page());
    }
  })
  .listen(8080);

function getFavIcon() {
  return "ico";
}

function getIndexPage() {
  return "Index Page";
}

function getInnerPage() {
  return "Inner Page";
}

function get404Page() {
  return "404 page not found";
}
